-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.7.12-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema greeting_card
--

CREATE DATABASE IF NOT EXISTS greeting_card;
USE greeting_card;

--
-- Definition of table `greeting_card`
--

DROP TABLE IF EXISTS `greeting_card`;
CREATE TABLE `greeting_card` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template_id` int(10) unsigned NOT NULL,
  `is_deleted` bit(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_greeting_card_1` (`template_id`),
  KEY `FK_greeting_card_2` (`user_id`),
  CONSTRAINT `FK_greeting_card_1` FOREIGN KEY (`template_id`) REFERENCES `template` (`id`),
  CONSTRAINT `FK_greeting_card_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `greeting_card`
--

/*!40000 ALTER TABLE `greeting_card` DISABLE KEYS */;
INSERT INTO `greeting_card` (`id`,`template_id`,`is_deleted`,`created_at`,`user_id`) VALUES 
 (1,5,0x00,'2017-07-08 21:28:39',1),
 (2,5,0x00,'2017-07-08 22:51:54',1),
 (3,5,0x00,'2017-07-08 22:55:07',1),
 (4,5,0x00,'2017-07-08 22:56:01',1),
 (5,5,0x00,'2017-07-09 01:14:53',1),
 (7,5,0x00,'2017-07-09 19:35:02',1),
 (8,5,0x00,'2017-07-09 21:34:20',1),
 (9,5,0x00,'2017-07-10 00:12:27',1);
/*!40000 ALTER TABLE `greeting_card` ENABLE KEYS */;


--
-- Definition of table `greeting_card_value`
--

DROP TABLE IF EXISTS `greeting_card_value`;
CREATE TABLE `greeting_card_value` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template_param_id` int(10) unsigned NOT NULL,
  `is_deleted` bit(1) NOT NULL,
  `value` varchar(100) NOT NULL,
  `greeting_card_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_greeting_card_value_1` (`template_param_id`),
  KEY `FK_greeting_card_value_2` (`greeting_card_id`),
  CONSTRAINT `FK_greeting_card_value_1` FOREIGN KEY (`template_param_id`) REFERENCES `template_param` (`id`),
  CONSTRAINT `FK_greeting_card_value_2` FOREIGN KEY (`greeting_card_id`) REFERENCES `greeting_card` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `greeting_card_value`
--

/*!40000 ALTER TABLE `greeting_card_value` DISABLE KEYS */;
INSERT INTO `greeting_card_value` (`id`,`template_param_id`,`is_deleted`,`value`,`greeting_card_id`) VALUES 
 (1,1,0x00,'g',7),
 (2,1,0x00,'g',8),
 (3,1,0x00,'g',9);
/*!40000 ALTER TABLE `greeting_card_value` ENABLE KEYS */;


--
-- Definition of table `template`
--

DROP TABLE IF EXISTS `template`;
CREATE TABLE `template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `is_deleted` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `template`
--

/*!40000 ALTER TABLE `template` DISABLE KEYS */;
INSERT INTO `template` (`id`,`name`,`is_deleted`) VALUES 
 (5,'t1',0x00),
 (6,'t1',0x00),
 (7,'t2',0x00),
 (8,'t2',0x00),
 (9,'t2',0x00),
 (10,'t2',0x00),
 (11,'t2',0x00),
 (12,'t2',0x00);
/*!40000 ALTER TABLE `template` ENABLE KEYS */;


--
-- Definition of table `template_param`
--

DROP TABLE IF EXISTS `template_param`;
CREATE TABLE `template_param` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `is_deleted` bit(1) NOT NULL,
  `template_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_template_param_1` (`template_id`),
  CONSTRAINT `FK_template_param_1` FOREIGN KEY (`template_id`) REFERENCES `template` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `template_param`
--

/*!40000 ALTER TABLE `template_param` DISABLE KEYS */;
INSERT INTO `template_param` (`id`,`name`,`is_deleted`,`template_id`) VALUES 
 (1,'tp1',0x00,5),
 (2,'tp1',0x00,6),
 (3,'tp1',0x00,7),
 (4,'tp1',0x00,8),
 (5,'tp1',0x00,9),
 (6,'tp1',0x00,10),
 (7,'tp1',0x00,11),
 (8,'tp1',0x00,12);
/*!40000 ALTER TABLE `template_param` ENABLE KEYS */;


--
-- Definition of table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `is_deleted` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`,`username`,`password`,`is_deleted`) VALUES 
 (1,'noam','123456',0x00);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
