<%@ page language="java" contentType="text/html; charset=windows-1255"
    pageEncoding="windows-1255"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>NewTone Music</title>

	<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <!-- Bootstrap core CSS -->
    <link href="static/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="static/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="static/assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="static/assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="static/assets/lineicons/style.css">    
    
    <link href="static/assets/css/style.css" rel="stylesheet">
    <link href="static/assets/css/style-responsive.css" rel="stylesheet">
	<link  href="static/assets/css/bootstrap-slider.css">

    <script src="static/assets/js/chart-master/Chart.js"></script>
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOGhtCB3NWKgEm-xHxFDYAeSdhhXwoqu8&sensor=false&extension=.js"></script>
	
    <script src="static/assets/js/jquery-1.8.3.min.js"></script>
    <script src="static/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="static/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="static/assets/js/jquery.scrollTo.min.js"></script>
    <script src="static/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="static/assets/js/jquery.sparkline.js"></script>
	<script type='text/javascript' src='static/assets/js/moment.js'></script>
	
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.11/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.11/angular-route.js"></script>
	<script type='text/javascript' src='static/assets/js/angular/ng-infinite-scroll.min.js'></script>
	<script type='text/javascript' src='static/assets/js/angular/angular-sanitize.min.js'></script>

	<script type='text/javascript' src='static/assets/js/slider.js'></script>
	<script type='text/javascript' src='static/assets/js/bootstrap-slider.js'></script>
	<script type='text/javascript'  src="static/assets/js/bootstrap-switch.js"></script>
	<script type='text/javascript'  src="static/assets/js/angular/angular-bootstrap-switch.js"></script>
	<script type='text/javascript'  src="static/assets/js/angular/bsSwitch.js"></script>
	
	<script type="text/javascript" src="static/js/app.js"></script>	
	<script type="text/javascript" src="static/js/greetingCardService.js"></script>
	<script type="text/javascript" src="static/js/dashboardCtrl.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" ng-app="dashboard" ng-controller="dashboardCtrl">

   
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <div class="row">                      
                  <!-- angular templating -->                  
		  <div></div>
              </div>    
            </section>
	</section>

</section>

  </body>
</html>
