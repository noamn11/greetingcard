<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!doctype html>
<!--[if lt IE 7]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <title>NewTone Music</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="static/app/css/bootstrap.min.css">
    <link rel="stylesheet" href="static/app/css/animate.css">
    <link rel="stylesheet" href="static/app/css/font-awesome.min.css">
    <link rel="stylesheet" href="static/app/css/owl.carousel.css">
    <link rel="stylesheet" href="static/app/css/owl.theme.css">
    <link rel="stylesheet" href="static/app/css/styles.css">
    <link href="static/assets/css/style.css" rel="stylesheet">
    <link href="static/assets/css/style-responsive.css" rel="stylesheet">
    <script src="static/app/js/modernizr.custom.32033.js"></script>
<script type='text/JavaScript' src="static/app/js/jquery.min.js"></script>
    <!--[if IE]><script type="text/javascript" src="js/excanvas.compiled.js"></script><![endif]-->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<script type="text/javascript" src="static/assets/js/jquery.backstretch.min.js"></script>
</head>

<body>

	    		<div id="login-page">
	    			<div class="container">
						<div class="login-form">
							<c:url var="loginUrl" value="index" />
							<form action="${loginUrl}" method="post" class="form-login">
						        <h2 class="form-login-heading">Login</h2>
						        <c:if test="${param.error != null}">
									<div class="alert alert-danger">
										<p>Invalid username and password.</p>
									</div>
								</c:if>
								<c:if test="${param.logout != null}">
									<div class="alert alert-success">
										<p>You have been logged out successfully.</p>
									</div>
								</c:if>
						        <div class="login-wrap">
						            <input type="text" class="form-control" id="username" name="ssoId" placeholder="Enter Username" required>
						            <br>
						            <input type="password" class="form-control" placeholder="Password" id="password" name="password" required>
						            <label class="checkbox">
						                <span class="pull-right">
						                    <a data-toggle="modal" href="login.html#myModal"> Forgot Password?</a>						
						                </span>
						            </label>
						            <button class="btn btn-theme-login btn-block" href="index.html" type="submit"><i class="fa fa-music"></i> Login</button>
						            <hr>
						            
						            <div class="login-social-link centered">
						            <p>or you can sign in via your social network</p>
						                <button class="btn btn-facebook" type="submit"><i class="fa fa-facebook"></i> Facebook</button>
						                <button class="btn btn-twitter" type="submit"><i class="fa fa-twitter"></i> Twitter</button>
						            </div>
						            <div class="registration">
						                Don't have an account yet?<br/>
						                <a class="login-txt" href="signup">
						                    Create an account
						                </a>
						            </div>
						
						        </div>
						
						          <!-- Modal -->
						          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
						              <div class="modal-dialog">
						                  <div class="modal-content">
						                      <div class="modal-header">
						                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						                          <h4 class="modal-title">Forgot Password ?</h4>
						                      </div>
						                      <div class="modal-body">
						                          <p>Enter your e-mail address below to reset your password.</p>
						                          <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
						
						                      </div>
						                      <div class="modal-footer">
						                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
						                          <button class="btn btn-theme-login" type="button">Submit</button>
						                      </div>
						                  </div>
						              </div>
						          </div>
						          <!-- modal -->
									<input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
						      </form>
												
						</div>
					</div>
				</div>
				
    			
    <script src="static/app/js/bootstrap.min.js"></script>
    <script src="static/app/js/owl.carousel.min.js"></script>
    <script src="static/app/js/waypoints.min.js"></script>

    <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASm3CwaK9qtcZEWYa-iQwHaGi3gcosAJc&sensor=false"></script>-->

    <!-- jQuery REVOLUTION Slider  -->
    <script type="text/javascript" src="static/app/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="static/app/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script type='text/JavaScript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.3.8/packaged/jquery.noty.packaged.min.js"></script>
	
    <script src="static/app/js/script.js"></script>
    <script>
        $(document).ready(function() {
            appMaster.preLoader();
        });

        function moveToLogin(){
			window.location.href = "login"
        }
    </script>

</body>

</html>