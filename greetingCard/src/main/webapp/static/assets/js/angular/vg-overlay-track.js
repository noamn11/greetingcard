/**
 * @license videogular v1.4.4 http://videogular.com
 * Two Fucking Developers http://twofuckingdevelopers.com
 * License: MIT
 */
/**
 * @ngdoc directive
 * @name com.2fdevs.videogular.plugins.overlayplay.directive:vgOverlayPlay
 * @restrict E
 * @description
 * Shows a big play button centered when player is paused or stopped.
 *
 * <pre>
 * <videogular vg-theme="config.theme.url" vg-autoplay="config.autoPlay">
 *    <vg-media vg-src="sources"></vg-media>
 *
 *    <vg-overlay-play></vg-overlay-play>
 * </videogular>
 * </pre>
 *
 */
"use strict";
angular.module("com.2fdevs.videogular.plugins.overlaytrack", [])
    .run(
        ["$templateCache", function ($templateCache) {
            $templateCache.put("vg-templates/vg-overlay-track",
            	'<div class="actions" style="position:absolute;top:10px;right:2.5%;font-size:14px;cursor:pointer" class="dropdown" >\
        			<a data-toggle="dropdown" class="dropdown-toggle" href="index.html#" style="color:#fff;">\
            				<i class="fa fa-ellipsis-v"></i>\
                   		 </a>\
                    		<ul class="dropdown-menu extended">\
        	                    <div class="notify-arrow notify-arrow-green"></div>\
        	                    <li ng-click="removeStreamTrack(songReport)">\
        	                        <p class="green">Remove</p>\
        	                    </li>\
        	                </ul>\
        		 </div>\
            	  <div class="overlay" ></div>\
                  <div class="iconButton" ng-class="overlayPlayIcon" ng-click="onClickOverlayPlay()"></div>\
            	   <div class="overlayTrackContainer" >\
                   <div class="name-div" >{{formatTrackDisplay(songReport)}}</div>\
            	   <div class="row user-div" style="width:50%;margin-top:5px;">\
						<div class="col-md-4 col-sm-4" style="position:relative;padding-left:0px;">\
							<i class="fa fa-eye" aria-hidden="true"></i>\
							<span style="font-size:14px;">{{songReport.count}}</span>\
						</div>\
						<div class="col-md-4 col-sm-4" style="position:relative;padding-left:0px;">\
							<i class="fa fa-heart" aria-hidden="true" ng-class="getLikeCss(songReport.songId)" ng-click="likeTrack(songReport.songId)"></i>\
							<span style="font-size:14px;">{{songReport.likes}}</span>\
						</div>\
						<div class="col-md-4 col-sm-4" style="position:relative;padding-left:0px;">\
							<i class="fa fa-share" aria-hidden="true" ng-click="shareTrack(songReport)"></i>\
						</div>\
          		 </div>\
            </div>');
            
            $templateCache.put("vg-templates/vg-overlay-track-limited",
                	'<div class="iconButton" ng-class="overlayPlayIcon" ng-click="onClickOverlayPlay()"></div>\
                	   <div class="overlayTrackContainer" >\
                       <div class="name-div" >{{formatTrackDisplay(songReport)}}</div>\
                	   <div class="row user-div" style="width:50%;margin-top:5px;">\
    						<div class="col-md-4 col-sm-4" style="position:relative;padding-left:0px;">\
    							<i class="fa fa-eye" aria-hidden="true"></i>\
    							<span style="font-size:14px;">{{songReport.count}}</span>\
    						</div>\
    						<div class="col-md-4 col-sm-4" style="position:relative;padding-left:0px;">\
    							<i class="fa fa-heart" aria-hidden="true" ng-click="likeTrack(songReport.songId)"></i>\
    							<span style="font-size:14px;">{{songReport.likes}}</span>\
    						</div>\
    						<div class="col-md-4 col-sm-4" style="position:relative;padding-left:0px;">\
    							<i class="fa fa-share" aria-hidden="true" ng-click="shareTrack(songReport)"></i>\
    						</div>\
              		 </div>\
                </div>');
        }]
    )
    .directive("vgOverlayTrack", ["VG_STATES","tracksService", "userService",
        function (VG_STATES,tracksService, userService) {
            return {
                restrict: "E",
                require: "^videogular",
                scope: {songReport:"="},
                templateUrl: function (elem, attrs) {
                	var tmplt = null;
                	if(attrs.vgTemplate){
                		tmplt = attrs.vgTemplate;
                	}else if(attrs.limited){
                		tmplt = 'vg-templates/vg-overlay-track-limited';
                	}
                	else{
                		tmplt = 'vg-templates/vg-overlay-track';
                	}
                	return tmplt;
                },
                link: function (scope, elem, attr, API) {
                    scope.onChangeState = function onChangeState(newState) {
                        switch (newState) {
                            case VG_STATES.PLAY:
                                scope.overlayPlayIcon = {};
                                break;

                            case VG_STATES.PAUSE:
                                scope.overlayPlayIcon = {play: true};
                                break;

                            case VG_STATES.STOP:
                                scope.overlayPlayIcon = {play: true};
                                break;
                        }
                    };
                    
                    scope.formatTrackDisplay = function(track){
                    	return tracksService.formatTrackDisplay(track);
                	};
                	
                	scope.removeStreamTrack = function(track){
                    	tracksService.removeStreamTrack(track);
                	};
                	
                	scope.likeTrack = function(trackId){
                		userService.likeSong(trackId);
                	};
                	
                	scope.onClickOverlayPlay = function onClickOverlayPlay(event) {
                        API.playPause();
                    };

                    scope.overlayPlayIcon = {play: true};
                	
                    scope.$watch(
                        function () {
                            return API.currentState;
                        },
                        function (newVal, oldVal) {
                            scope.onChangeState(newVal);
                        }
                    );
                    
                    scope.getLikeCss = function(songId){
                		var user = userService.getUserObj();
                		if(userService.isSongLikedByUser(user, songId)){
                			return "icon-theme2";
                		}
                		else return "";
                	};
                	
                	scope.shareTrack = function(songReport){
                		tracksService.shareTrack(songReport, userService.getUserObj().name);
                	};
                }
            }
        }
    ]);

