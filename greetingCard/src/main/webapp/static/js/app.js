
var app = angular.module('dashboard', ['ngRoute']);

app.config(function($routeProvider) {
    $routeProvider
        // route for the home page
        .when('/', {
            templateUrl : 'static/assets/views/stream.html',
            controller  : 'streamCtrl'
        })
        .when('/myplaylist', {
            templateUrl : 'static/assets/views/myplaylist.html',
            controller  : 'playlistCtrl'
        })
        .when('/map', {
            templateUrl : 'static/assets/views/map.html',
            controller  : 'mapCtrl'
        })
	    .when('/followed', {
	        templateUrl : 'static/assets/views/followedUsers.html',
	        controller  : 'dashboardCtrl'
	    })
	    .when('/addUsers', {
	        templateUrl : 'static/assets/views/addUsers.html',
	        controller  : 'dashboardCtrl'
	    })
    	.when('/track', {
    		templateUrl : 'static/assets/views/track.html',
    		controller  : 'dashboardCtrl'
    	}).when('/settings', {
    		templateUrl : 'static/assets/views/settings.html',
    		controller  : 'dashboardCtrl'
		}).when('/favorites', {
			templateUrl : 'static/assets/views/favorites.html',
			controller  : 'favoritesCtrl'
		})
    	.when('/user/:id/', {
		    templateUrl: 'static/assets/views/user.html',    
		    controller: 'userCtrl'
		});
})



app.service("commons", ['$http','$window', '$filter', function ($http, $window, $filter) {
	this.createDuoArr = function(list){
		var arr = [];
		var count = 0;
		if(list){
			for(var i = 0 ; i < list.length ; ){
                            var duo = [];    		
                            duo[0] = list[i];
                            if(i+1 < list.length){
                                    duo[1] =list[i+1];
                            }
                            arr[count++] = duo;
                            i = i+2;
			}
		}		
		return arr;
	};
        this.showLoaderSpinner = false;
	this.page = 0;
	this.scrollY = 0;
	this.defaultScrollAmount = 850;
        this.tracksFilter = "-timestamp";
        
        this.tracksFilters = [
            {
              name: 'Time',
              val: 'timestamp',
            }, {
              name: 'Views',
              val: 'count',
            }, {
              name: 'Likes',
              val: 'likes',
            }
        ];
        this.tracksFilterSelected = this.tracksFilters[0];
        
        this.sortTracks = function(tracks){
           return  $filter('orderBy')(tracks, this.tracksFilter);
        };
        
	this.isWithinScrollAmount = function(busy){
		return  busy ||$window.scrollY - this.scrollY < this.defaultScrollAmount; 
	};
	
	this.formatTime = function(time){
            var past = new Date(time);
            var pastObj = moment(past);
            var nowCal = new Date();
            pastObj.add(1, "days");
            if(pastObj.isAfter(nowCal)){
                var dif =  pastObj.diff(nowCal, 'hours') *-1;
                return dif+"h";	
            }else{
                    pastObj.add(-1, "days");
                    pastObj.add(1, "hours");
                if(pastObj.isAfter(nowCal)){
                    var dif =  pastObj.diff(nowCal, 'minutes')*-1 ;
                    return dif+"m";	
                }else{
                    var dif =  pastObj.diff(nowCal, 'day') *-1;
                    return dif+"d";	
                }
            }
        };
        
        this.createVideoSource = function(songReport){
            var source;
            if(songReport.url.indexOf("youtube") !== -1){
                    source = [{"src":new String(songReport.url)}];		    		
            }else{
                    source = [{"src":new String(songReport.url), type: "video/ogg"}];
            }
            return source;
        };
        
        this.getSongReports = function (songReports, idx) {
            if (idx == 0){
                return songReports.slice(idx, idx+2);

            }else{
                return songReports.slice(idx*2, (idx*2)+2);
            }
        };
        
        
        this.showLoader = function(val){
            this.showLoaderSpinner = val;
        };
        
        this.getShowLoader= function(){
            return this.showLoaderSpinner;
        };

	
}]);
