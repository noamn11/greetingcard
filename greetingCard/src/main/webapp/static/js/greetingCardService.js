app.service("greetingCardService", ['$http',function ($http) {
  

	
        this.saveTemplate = function(template){
                var templateStr = JSON.stringify(template);
		var context = this;
		$http({
                    method : "PUT",
                    data: templateStr,
                    url: "greetingCard/template",
                }).then(function mySucces(response) {
                    
                }, function myError(response) {

                });    			
	};
        
        this.saveGreetingCard = function(templateId){

		$http({
                    method : "POST",
                    //data: templateStr,
                    url: "greetingCard/template/"+templateId+"?tp1=g&tp2=h",
                }).then(function mySucces(response) {
                    
                }, function myError(response) {

                });    			
	};
        
        this.getTemplates = function(){

		$http({
                    method : "GET",
                    url: "greetingCard/catalog/",
                }).then(function mySucces(response) {
                    
                }, function myError(response) {

                });    			
	};
	
}]);
