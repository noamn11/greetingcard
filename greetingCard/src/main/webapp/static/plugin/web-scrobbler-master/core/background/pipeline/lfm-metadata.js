'use strict';

define([
	'services/newtone'
], function(Newtone) {

	return {
		loadSong: function (song, cb) {
			var onLoaded = function (isLfmValid) {
				cb(isLfmValid);
			};

			// load song metadata and update attemptedLFMValidation flag
			Newtone.loadSongInfo(song, onLoaded);
		}
	};

});
