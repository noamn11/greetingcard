package com.websystique.springmvc.controller;

import com.websystique.springmvc.greetingcard.model.GreetingCard;
import com.websystique.springmvc.greetingcard.service.GreetingCardsService;
import com.websystique.springmvc.greetingcard.template.Template;
import com.websystique.springmvc.greetingcard.template.TemplateParam;
import com.websystique.springmvc.greetingcard.user.service.UserService;
import com.websystique.springmvc.greetingcard.user.model.User;
import java.security.Principal;
import java.util.LinkedHashSet;

import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;


@Controller
@RequestMapping("/")
public class AppController {
	
        @Autowired
        UserService userService;
        
        @Autowired
        GreetingCardsService greetingCardsService;       
        
        private static final String INDEX = "index";
        private static final String MAIN = "Theme/dashboard";
	
	@RequestMapping(value = { "/index"}, method = RequestMethod.GET)
	public String index(ModelMap model) {
		return INDEX;  
	}
        	
	
	@RequestMapping(value = { "/"}, method = RequestMethod.GET)
	public String getHome(ModelMap model, Principal principal) {
		final String loggedInUserName = principal.getName();
		if(loggedInUserName != null && !loggedInUserName.equals("")){
			return MAIN;  
		}else{
			return INDEX;  
		}		
	}
	

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage(ModelMap model) {
		return INDEX;
	}	
}