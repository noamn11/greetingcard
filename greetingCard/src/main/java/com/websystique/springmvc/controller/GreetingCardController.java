package com.websystique.springmvc.controller;

import com.websystique.springmvc.greetingcard.model.GreetingCard;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.websystique.springmvc.greetingcard.service.GreetingCardsService;
import com.websystique.springmvc.greetingcard.template.Template;
import com.websystique.springmvc.greetingcard.template.TemplateParam;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;



@Controller
@RequestMapping("/greetingCard")
public class GreetingCardController {

        @Autowired
        GreetingCardsService greetingCardsService;
	
	@Secured(value = { "ROLE_ADMIN","ROLE_USER"})
        @RequestMapping(value="/catalog/", method = RequestMethod.GET)
	public @ResponseBody List<Template> findAllTemplates (){
		List<Template> templates = greetingCardsService.findAllTemplates();
                return templates;
	} 
        
        @Secured(value = { "ROLE_ADMIN","ROLE_USER"})
	@RequestMapping(value="/template/{templateId}", method = RequestMethod.POST)
	public @ResponseBody GreetingCard saveGreetingCard (HttpServletRequest request, HttpServletResponse response, @PathVariable int templateId){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null){    
                    String username = auth.getName();
                    return greetingCardsService.saveGreetingCard(username, templateId, request.getParameterMap());
		}
                else return null;
	}
                
        
        @Secured(value = { "ROLE_ADMIN"})
        @RequestMapping(value = { "/template" }, method = RequestMethod.PUT)
	public @ResponseBody Template saveTemplate(@RequestBody @Valid Template template) {            
            greetingCardsService.saveTemplate(template);
            return template;
        } 
                
                
}