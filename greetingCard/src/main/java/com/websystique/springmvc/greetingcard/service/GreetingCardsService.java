/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.websystique.springmvc.greetingcard.service;

import com.websystique.springmvc.greetingcard.dao.GreetingCardRepository;
import com.websystique.springmvc.greetingcard.model.GreetingCard;
import com.websystique.springmvc.greetingcard.model.GreetingCardValue;
import com.websystique.springmvc.greetingcard.template.Template;
import com.websystique.springmvc.greetingcard.template.TemplateParam;
import com.websystique.springmvc.greetingcard.template.dao.TemplateRepository;
import com.websystique.springmvc.greetingcard.user.dao.UserRepository;
import com.websystique.springmvc.greetingcard.user.model.User;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author NOAM
 */
@Service
@Transactional
public class GreetingCardsService {
    
 
    @Autowired
    TemplateRepository templateRepository;
    
    @Autowired
    UserRepository userRepository;
    
    @Autowired
    GreetingCardRepository greetingCardRepository;
    
    private static final String UNKNOWN_TEMPLATE = "Unknown template id %d";
    private static final String UNKNOWN_TEMPLATE_PARAM = "Unknown template param %s";
    private static final String UNKNOWN_USER = "Unknown user id %d";
    private static final boolean ACTIVE_USERS = false;
    
    static Logger log = Logger.getLogger(GreetingCardsService.class.getName());

    public List<Template> findAllTemplates(){
       return templateRepository.findAllByIsDeleted(ACTIVE_USERS);
        
    }
    
 
    public GreetingCard saveGreetingCard(String username, int templateId, Map<String,String[]> params){
        User user = userRepository.findByUsername(username);
        Template template = templateRepository.findOne(templateId);
        if(template == null){
            throw new IllegalArgumentException(UNKNOWN_TEMPLATE);
        }
        Set<GreetingCardValue> greetingCardValues = new LinkedHashSet();
        for(TemplateParam param: template.getTemplateParams()){
            if(params.get(param.getName()) != null){
                GreetingCardValue greetingCardValue = new GreetingCardValue();
                greetingCardValue.setValue(params.get(param.getName())[0]);
                greetingCardValue.setTemplateParam(param);               
                greetingCardValues.add(greetingCardValue);
            }else{
                throw new IllegalArgumentException(String.format(UNKNOWN_TEMPLATE_PARAM, param.getName()));
            }
        }
        return saveGreetingCard( user, template, greetingCardValues);
    }
    
    public GreetingCard saveGreetingCard(User user, Template template, Set<GreetingCardValue> greetingCardValues){
        GreetingCard greetingCard = new GreetingCard();
        greetingCard.setCreatedAt(new Date());
        greetingCard.setTemplate(template);
        greetingCard.setUser(user);        
        for(GreetingCardValue gcv : greetingCardValues){
            gcv.setGreetingCard(greetingCard);
        }
        greetingCard.setGreetingCardsValues(greetingCardValues);
        greetingCardRepository.save(greetingCard);
        return greetingCard;
    }
    
    public GreetingCard saveGreetingCard(int userId, int templateId){
        Template template = templateRepository.findOne(templateId);
        if(template == null){
            throw new IllegalArgumentException(String.format(UNKNOWN_TEMPLATE, templateId));
        }
        
        User user = userRepository.findOne(userId);
        if(user == null){
            throw new IllegalArgumentException(String.format(UNKNOWN_USER, userId));
        }
        return saveGreetingCard(user, template, null);
    }
    
    public Template saveTemplate(String name, List<String> params){
        
        Template template = new Template();
        template.setName(name);
        params.stream().map((param) -> {
            TemplateParam tp = new TemplateParam();
            tp.setName(param);
            return tp;
        }).map((tp) -> {
            tp.setTemplate(template);
            return tp;
        }).forEachOrdered((tp) -> {
            template.getTemplateParams().add(tp);
        });
        templateRepository.save(template);
        return template;
    }
    
    public Template saveTemplate(Template template){
        try{           
            for(TemplateParam tp: template.getTemplateParams()){
                tp.setTemplate(template);
            }
            templateRepository.save(template);
        }catch(Exception e){
            e.printStackTrace();
        }       
        return template;
    }


}
