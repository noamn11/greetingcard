/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.websystique.springmvc.greetingcard.model;

import com.websystique.springmvc.greetingcard.template.TemplateParam;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author NOAM
 */
@Entity
@Table(name = "greeting_card_value")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
public class GreetingCardValue  implements Serializable{
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "value")
    private String value;
    
    @JoinColumn(name = "template_param_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private TemplateParam templateParam;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_deleted")
    private boolean isDeleted;
    
    @JoinColumn(name = "greeting_card_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private GreetingCard greetingCard;
    
    public GreetingCardValue(){}

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the isDeleted
     */
    public boolean isDeleted() {
        return isIsDeleted();
    }

    /**
     * @param isDeleted the isDeleted to set
     */
    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * @return the templateParam
     */
    public TemplateParam getTemplateParam() {
        return templateParam;
    }

    /**
     * @param templateParam the templateParam to set
     */
    public void setTemplateParam(TemplateParam templateParam) {
        this.templateParam = templateParam;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the isDeleted
     */
    public boolean isIsDeleted() {
        return isDeleted;
    }

    /**
     * @return the greetingCard
     */
    public GreetingCard getGreetingCard() {
        return greetingCard;
    }

    /**
     * @param greetingCard the greetingCard to set
     */
    public void setGreetingCard(GreetingCard greetingCard) {
        this.greetingCard = greetingCard;
    }
    
    
    
}
