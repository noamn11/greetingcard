/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.websystique.springmvc.greetingcard.user.service;

import com.websystique.springmvc.greetingcard.user.dao.UserRepository;
import com.websystique.springmvc.greetingcard.user.model.User;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author NOAM
 */
@Service
@Transactional
public class UserService {
    
    @Autowired
    UserRepository userRepository;
    
    
    public User addUser(User u){
        try{
            u.setId(null);
            u.setPassword("123456");
            User user = userRepository.save(u);
            return user;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    public User findById(int id){
        User u =  userRepository.findOne(id);

        return u;
    }
    
    
}
