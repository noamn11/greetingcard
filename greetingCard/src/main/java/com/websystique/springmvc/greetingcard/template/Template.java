/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.websystique.springmvc.greetingcard.template;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.websystique.springmvc.greetingcard.model.GreetingCard;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author NOAM
 */
@Entity
@Table(name = "template")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@templateId")
public class Template  implements Serializable{
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_deleted")
    private boolean isDeleted;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "template", fetch = FetchType.LAZY)
    private Set<TemplateParam> TemplateParams = new LinkedHashSet();
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "template", fetch = FetchType.LAZY)
    private Set<GreetingCard> GreetingCards = new LinkedHashSet();
    
    public Template(){}

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the isDeleted
     */
    public boolean isDeleted() {
        return isDeleted;
    }

    /**
     * @param isDeleted the isDeleted to set
     */
    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    
    /**
     * @return the TemplateParams
     */
    public Set<TemplateParam> getTemplateParams() {
        return TemplateParams;
    }

    /**
     * @param TemplateParams the TemplateParams to set
     */
    public void setTemplateParams(Set<TemplateParam> TemplateParams) {
        this.TemplateParams = TemplateParams;
    }

    /**
     * @return the GreetingCards
     */
    public Set<GreetingCard> getGreetingCards() {
        return GreetingCards;
    }

    /**
     * @param GreetingCards the GreetingCards to set
     */
    public void setGreetingCards(Set<GreetingCard> GreetingCards) {
        this.GreetingCards = GreetingCards;
    }
    
    
    
}
