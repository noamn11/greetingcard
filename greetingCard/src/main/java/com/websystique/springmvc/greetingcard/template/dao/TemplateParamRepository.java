/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.websystique.springmvc.greetingcard.template.dao;

import com.websystique.springmvc.greetingcard.template.TemplateParam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author NOAM
 */
@Repository
public interface TemplateParamRepository extends JpaRepository<TemplateParam, Integer> {
}