package com.websystique.springmvc.exception;


import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.MethodArgumentNotValidException;
/**
 *
 * @author Noamn11
 */
@ControllerAdvice
public class GeneralExceptionHandler {
    @ExceptionHandler(Exception.class)
    public @ResponseBody ResponseEntity<String> defaultErrorHandler(Throwable e){
    	System.out.println("defaultErrorHandler:: err="+e.getMessage());
        return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        
    }
    
    @ExceptionHandler(IOException.class)
    public @ResponseBody ResponseEntity<String> errorHandler(Throwable e){
    	System.out.println("errorHandler:: err="+e.getMessage());
        return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        
    }
    
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public @ResponseBody ResponseEntity<String> invalidErrorHandler(Throwable e){
    	System.out.println("invalidErrorHandler:: err="+e.getMessage());
        return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
        
    }
    
}
