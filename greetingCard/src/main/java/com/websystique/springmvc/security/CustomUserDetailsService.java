package com.websystique.springmvc.security;

import com.websystique.springmvc.greetingcard.user.dao.UserRepository;
import com.websystique.springmvc.greetingcard.user.model.User;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService{

	static final Logger logger = LoggerFactory.getLogger(CustomUserDetailsService.class);
	
        @PersistenceContext
        private EntityManager entityManager;
        
        @Autowired
        private UserRepository repository;
	
	@Transactional(readOnly=true)
	public UserDetails loadUserByUsername(String ssoId) throws UsernameNotFoundException {
		List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();
		System.out.println("loadUserByUsername::ssoId = "+ssoId);
		if(entityManager == null){
			System.out.println("entityManager == null ");
		}else{
			System.out.println("entityManager != null ");
		}

		User  user = repository.findByUsername(ssoId);
		if(user == null ){
			System.out.println("dbUser == null ");
			throw new UsernameNotFoundException("Username not found");
		}else{
			System.out.println("dbUser="+user);  
			logger.info("User : {}", user);
		}

		String role = "ROLE_ADMIN";
        //System.out.println("role="+role);
        list.add(new SimpleGrantedAuthority(role));
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), 
				 true, true, true, true, list);
                    
	}

	
//	private List<GrantedAuthority> getGrantedAuthorities(User user){
//		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
//		
//		for(UserProfile userProfile : user.getUserProfiles()){
//			logger.info("UserProfile : {}", userProfile);
//			authorities.add(new SimpleGrantedAuthority("ROLE_"+userProfile.getType()));
//		}
//		logger.info("authorities : {}", authorities);
//		return authorities;
//	}
	
}
